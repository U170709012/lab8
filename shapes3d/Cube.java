package shapes3d;

import shapes2d.Square;

public class Cube extends Square {
    public Cube() {
        super();
    }

    public Cube(double side) {
        super(side);
    }

    public double area() {
        return super.area() * 6;
    }

    public double volume() {
        return Math.pow(super.getSide(), 3);
    }

    public String toString() {
        return "Cube side is: " + super.getSide();
    }
}
